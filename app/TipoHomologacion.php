<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoHomologacion extends Model
{
    protected $table = 'tipo_homologacion';
}
